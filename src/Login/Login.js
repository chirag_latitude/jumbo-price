import React, { Component, history } from "react";
import "./Login.scss";
import logo from "./../public/images/Login/logo.svg";
import axios from "axios";

// import { symbol } from "prop-types";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Username: "",
      Password: ""
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  data;
  state = {
    loading: true
  };

  componentDidMount(a, b) {
    // let x = ;
    // const BODY = ;
  }
  onSubmit(e) {
    e.preventDefault();
    console.log(this.state.Username, this.state.Password);
    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    };
    let user = this.state.Username;
    let pass = this.state.Password;
    // let formData = new FormData();
    let url =
      "http://ec2-18-191-39-112.us-east-2.compute.amazonaws.com:8080/JumboPLPost/api/login";
    // formData.append("user", this.state.Username);
    // formData.append("pass", this.state.Password);
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ user, pass })
    };
    return fetch(`${url}`, requestOptions).then(user => {
      console.log(user);
      if (user.status == 200) {
        this.props.history.push("/Content");
      }
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem("user", JSON.stringify(user.status));

      return user;
    });
    // axios({
    //   method: "POST",
    //   url,
    //   data: formData,
    //   headers: { "Content-Type": "application/json" }
    // })
    //   .then(response => {
    //     console.log(response);
    //   })
    //   .catch(error => {
    //     console.log(error);
    //   });
    // axios
    //   .post(url, formData, config)
    //   .then(response => {
    //     console.log(response);
    //   })
    //   .catch(error => {
    //     console.log(error);
    //   });
    // return fetch(
    //   `http://ec2-18-191-39-112.us-east-2.compute.amazonaws.com:8080/JumboPLPost/api/login`,
    //   requestOptions
    // ).then(user => {
    //   console.log(user);
    //   // store user details and jwt token in local storage to keep user logged in between page refreshes
    //   //localStorage.setItem('user', JSON.stringify(user));
    //   return user;
    // });

    //
    // history.push("/Content");
  }

  render() {
    return (
      <div className="login-main">
        {/* {this.state.loading ? <div>loading...</div> : <div>person...</div>} */}
        <img src={logo} alt="Logo" className="logo" />

        <form onSubmit={this.onSubmit}>
          <h2>LOG IN</h2>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              id="email"
              placeholder="Username"
              value={this.state.Username}
              onChange={evt =>
                this.setState({
                  Username: evt.target.value
                })
              }
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              className="form-control"
              id="pwd"
              placeholder="Password"
              value={this.state.Password}
              onChange={evt =>
                this.setState({
                  Password: evt.target.value
                })
              }
            />
          </div>
          <div className="form-group form-check">
            <div className="checkbox">
              <input
                type="checkbox"
                id="checkbox_remember_me"
                name=""
                value=""
              />
              <label for="checkbox_remember_me">
                <span>Remember me</span>
              </label>
            </div>
            <a className="forgot_pass" href="ForgotPassword">
              Forgot Password
            </a>
          </div>
          <button type="submit" className="btn-login">
            Login
          </button>
        </form>
      </div>
    );
  }
}

export default Login;
