import React from "react";
import PropTypes from "prop-types";
import { lighten, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import "./Table.scss";

// import DeleteIcon from '@material-ui/icons/Delete';
// import FilterListIcon from '@material-ui/icons/FilterList';

function createData(
  description,
  jumbosku,
  jumboprice,
  costprice,
  carrefourprice,
  sharafdgprice,
  amazonprice,
  noonprice,
  datemodified,
  timemodified
) {
  return {
    description,
    jumbosku,
    jumboprice,
    costprice,
    carrefourprice,
    sharafdgprice,
    amazonprice,
    noonprice,
    datemodified,
    timemodified
  };
}

const rows = [
  createData(
    "APPLE IPHONE XR SMARTPHONE LTE WITH FACETIME, BLACK, 128 GB",
    "AHJYY89",
    3100.25,
    3100.25,
    3150.5,
    3150.5,
    3150.5,
    3150.75,
    "DD.MM.YYYY",
    "00.00.00"
  ),
  createData(
    "APPLE IPHONE XR SMARTPHONE LTE WITH FACETIME, BLACK, 64 GB",
    "AHJYY89",
    3100.25,
    3100.25,
    3150.5,
    3150.5,
    3150.5,
    3150.75,
    "DD.MM.YYYY",
    "00.00.00"
  ),
  createData(
    "APPLE IPHONE XR SMARTPHONE LTE WITH FACETIME, BLACK, 32 GB",
    "AHJYY89",
    3100.25,
    3100.25,
    3150.5,
    3150.5,
    3150.5,
    3150.75,
    "DD.MM.YYYY",
    "00.00.00"
  ),
  createData(
    "APPLE IPHONE XR SMARTPHONE LTE WITH FACETIME, BLACK, 24 GB",
    "AHJYY89",
    3100.25,
    3100.25,
    3150.5,
    3150.5,
    3150.5,
    3150.75,
    "DD.MM.YYYY",
    "00.00.00"
  ),
  createData(
    "APPLE IPHONE XR SMARTPHONE LTE WITH FACETIME, BLACK, 12 GB",
    "AHJYY89",
    3100.25,
    3100.25,
    3150.5,
    3150.5,
    3150.5,
    3150.75,
    "DD.MM.YYYY",
    "00.00.00"
  )
];

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const headCells = [
  {
    id: "description",
    numeric: false,
    disablePadding: true,
    label: "PRODUCT DESCRIPTION",
    textalignleft: true
  },
  {
    id: "jumbosku",
    numeric: false,
    disablePadding: false,
    label: "Jumbo SKU",
    textalignleft: false
  },
  {
    id: "jumboprice",
    numeric: true,
    disablePadding: false,
    label: "Jumbo Price",
    textalignleft: false
  },
  {
    id: "costprice",
    numeric: true,
    disablePadding: false,
    label: "Cost Price",
    textalignleft: false
  },
  {
    id: "carrefourprice",
    numeric: true,
    disablePadding: false,
    label: "Carrefour Price",
    textalignleft: false
  },
  {
    id: "sharafdgprice",
    numeric: true,
    disablePadding: false,
    label: "SharafDG Price",
    textalignleft: false
  },
  {
    id: "amazonprice",
    numeric: true,
    disablePadding: false,
    label: "Amazon Price",
    textalignleft: false
  },
  {
    id: "noonprice",
    numeric: true,
    disablePadding: false,
    label: "Noon Price",
    textalignleft: false
  },
  {
    id: "datemodified",
    numeric: false,
    disablePadding: false,
    label: "Date Modified",
    textalignleft: false
  },
  {
    id: "timemodified",
    numeric: false,
    disablePadding: false,
    label: "Time Modified",
    textalignleft: false
  }
];

function EnhancedTableHead(props) {
  const { classes } = props;
  //   const createSortHandler = property => event => {
  //     onRequestSort(event, property);
  //   };

  return (
    <TableHead className={classes.tablehead}>
      <TableRow>
        <TableCell
          padding="checkbox"
          className={classes.tableheadcheckbox}></TableCell>
        {headCells.map(headCell => (
          <TableCell
            className={classes.tabletitle}
            key={headCell.id}
            align={headCell.textalignleft ? "left" : "center"}
            padding={headCell.disablePadding ? "none" : "default"}>
            {headCell.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1)
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  spacer: {
    flex: "1 1 100%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  }
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return "";
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3)
  },
  toolbar: {
    boxShadow: "none"
  },
  tablehead: {
    background: "#e2e9f05c",
    "& tr th:nth-child(2)": {
      borderLeft: "none"
    }
  },
  tableheadcheckbox: {
    borderBottom: "none",
    borderLeft: "none",
    padding: "10px"
  },
  tabletitle: {
    fontWeight: "bold",
    color: "#174a84",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
    boxShadow: "none"
  },
  table: {
    minWidth: 750
  },
  row: {
    borderBottom: "none"
  },
  checkboxcell: {
    borderBottom: "none",
    minWidth: "40px",
    padding: "10px"
  },
  checkbox: {
    width: "13px",
    height: "13px",
    borderRadius: "3px",
    background: "#fff",
    color: "#0086f8 !important",
    borderBottom: "none"
  },
  description: {
    fontWeight: "500",
    color: "#164883",
    borderBottom: "none",
    padding: "10px"
  },
  costprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  jumbosku: {
    fontWeight: "500",
    textDecoration: "underline",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  jumboprice: {
    fontWeight: "500",
    textDecoration: "underline",
    textAlign: "center",
    color: "#00c853",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  carrefourprice: {
    fontWeight: "500",
    textDecoration: "underline",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  sharafdgprice: {
    fontWeight: "500",
    textDecoration: "underline",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  amazonprice: {
    fontWeight: "500",
    textDecoration: "underline",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  noonprice: {
    fontWeight: "500",
    textDecoration: "underline",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  datemodified: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  timemodified: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  tableWrapper: {
    overflowX: "auto"
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1
  }
}));

export default function EnhancedTable() {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("costprice");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  //   const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isDesc = orderBy === property && order === "desc";
    setOrder(isDesc ? "asc" : "desc");
    setOrderBy(property);
  };

  const handleSelectAllClick = event => {
    if (event.target.checked) {
      const newSelecteds = rows.map(n => n.description);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, description) => {
    const selectedIndex = selected.indexOf(description);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, description);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  //   const handleChangeDense = event => {
  //     setDense(event.target.checked);
  //   };

  const isSelected = description => selected.indexOf(description) !== -1;

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  const [pageSizes] = React.useState([5, 10, 15, 0]);

  return (
    <div className="table-main row">
      <Paper className={classes.paper}>
        <EnhancedTableToolbar
          numSelected={selected.length}
          className={classes.toolbar}
        />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.description);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row.description)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.description}
                      selected={isItemSelected}
                      className={classes.row}
                      style={
                        index % 2
                          ? { background: "#e2e9f05c" }
                          : { background: "#fff" }
                      }>
                      <TableCell
                        padding="checkbox"
                        className={classes.checkboxcell}>
                        <Checkbox
                          className={classes.checkbox}
                          checked={isItemSelected}
                          inputProps={{ "aria-labelledby": labelId }}
                        />
                      </TableCell>
                      <TableCell
                        className={classes.description}
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none">
                        {row.description}
                      </TableCell>

                      <TableCell align="right" className={classes.jumbosku}>
                        {row.jumbosku}
                      </TableCell>
                      <TableCell align="right" className={classes.jumboprice}>
                        {row.jumboprice}
                      </TableCell>
                      <TableCell align="right" className={classes.costprice}>
                        {row.costprice}
                      </TableCell>
                      <TableCell
                        align="right"
                        className={classes.carrefourprice}>
                        {row.carrefourprice}
                      </TableCell>
                      <TableCell
                        align="right"
                        className={classes.sharafdgprice}>
                        {row.sharafdgprice}
                      </TableCell>
                      <TableCell align="right" className={classes.amazonprice}>
                        {row.amazonprice}
                      </TableCell>
                      <TableCell align="right" className={classes.noonprice}>
                        {row.noonprice}
                      </TableCell>
                      <TableCell align="right" className={classes.datemodified}>
                        {row.datemodified}
                      </TableCell>
                      <TableCell align="right" className={classes.timemodified}>
                        {row.timemodified}
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "previous page"
          }}
          nextIconButtonProps={{
            "aria-label": "next page"
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}
