import React, { Component } from "react";
import { Tabs, Tab, TabPanel, TabList } from "react-web-tabs";
import "react-web-tabs/dist/react-web-tabs.css";
import "./Content.scss";
import Table from "../Table/Table";
import AuditTable from "../AuditTable/AuditTable";

import SidebarMenu from "../SidebarMenu/SidebarMenu";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
// import { makeStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/TextField";

import { withStyles } from "@material-ui/core/styles";
import DateRangePicker from "react-bootstrap-daterangepicker";
// you will need the css that comes with bootstrap@3. if you are using
// a tool like webpack, you can do the following:
import "bootstrap/dist/css/bootstrap.css";
// you will also need the css that comes with bootstrap-daterangepicker
import "bootstrap-daterangepicker/daterangepicker.css";
// import pricing_active from './../public/images/Content/pricing_active.svg';

// images
import icCal from "./../public/images/Content/ic_cal.svg";

// chart

import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

let minRate = 120;
const options = {
  title: {
    text: ""
  },
  xAxis: {
    type: "datetime"
  },
  yAxis: {
    title: {
      text: "Price (AED)"
    },
    plotLines: [
      {
        value: minRate,
        color: "gray",
        dashStyle: "longdash",
        width: 1,
        label: {
          text: "gry Line"
        }
      }
    ]
  },
  plotOptions: {
    series: {
      pointStart: Date.UTC(2019, 0, 1),
      pointInterval: 2592000000 // one day
    }
  },

  legend: {
    align: "right",
    verticalAlign: "top",
    layout: "vertical",
    // floating: true,
    position: "relative",
    useHTML: true,
    labelFormatter: function() {
      return (
        "<div style='background: " +
        this.color +
        ";float: left;width: 14px; height: 14px; border-radius: 2px;margin: 0px 5px 5px 5px;'> </div>" +
        this.name
      );
    },

    itemStyle: {
      color: "#164883",
      fontWeight: "normal",
      fontSize: "9px",
      textOverflow: ""
    }
  },
  series: [
    {
      name: "Jumbo",
      data: [
        29.9,
        71.5,
        106.4,
        129.2,
        144.0,
        176.0,
        135.6,
        148.5,
        316.4,
        294.1,
        195.6,
        154.4
      ],
      type: "spline",
      color: "#315496",
      marker: {
        fillColor: "#FFFFFF",
        symbol: "■",
        lineColor: "#55D8FE",
        lineWidth: 2
      }
    },
    {
      name: "Carrefour",
      data: [
        216.4,
        194.1,
        95.6,
        54.4,
        29.9,
        71.5,
        106.4,
        129.2,
        144.0,
        176.0,
        135.6,
        148.5
      ],
      type: "spline",
      color: "#fe0000",
      marker: {
        fillColor: "#FFFFFF",
        symbol: "■",
        lineColor: "#55D8FE",
        lineWidth: 2
      }
    },
    {
      name: "Amazon",
      data: [
        106.4,
        129.2,
        144.0,
        176.0,
        135.6,
        148.5,
        216.4,
        194.1,
        95.6,
        54.4,
        29.9,
        71.5
      ],
      type: "spline",
      color: "#012060",
      marker: {
        fillColor: "#FFFFFF",
        symbol: "■",
        lineColor: "#55D8FE",
        lineWidth: 2
      }
    },
    {
      name: "Sharaf DG",
      data: [
        54.4,
        29.9,
        71.5,
        106.4,
        129.2,
        144.0,
        176.0,
        135.6,
        148.5,
        216.4,
        194.1,
        95.6
      ],
      type: "spline",
      color: "#ffc000",
      marker: {
        fillColor: "#FFFFFF",
        symbol: "■",
        lineColor: "#55D8FE",
        lineWidth: 2
      }
    },
    {
      name: "Noon",
      data: [
        94.4,
        69.9,
        71.5,
        146.4,
        199.2,
        144.0,
        206.0,
        155.6,
        348.5,
        216.4,
        394.1,
        75.6
      ],
      type: "spline",
      color: "#ffff00",
      marker: {
        fillColor: "#FFFFFF",
        symbol: "■",
        lineColor: "#55D8FE",
        lineWidth: 2
      }
    }
  ],

  credits: {
    enabled: false
  },

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 1024
        },
        chartOptions: {
          legend: {
            layout: "horizontal",
            align: "center",
            verticalAlign: "bottom"
          }
        }
      }
    ]
  }
};

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  MenuItem: {
    background: "transparent !important",
    fontWeight: "normal",
    color: "#164883",
    borderBottom: "0.5px solid #00accd",
    paddingLeft: 0,
    paddingRight: 0,
    margin: "0px 15px",
    "&:hover": {
      background: "transparent"
    },
    "&:last-child": {
      borderBottom: "none"
    }
  },
  MenuItemSelected: {
    background: "transparent",
    fontWeight: 600,
    color: "#164883",
    borderBottom: "0.5px solid #00accd"
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  }
});

class Content extends Component {
  state = {
    selected: "APPLE",
    pricingselected: "Pricing",
    hasError: false
  };

  handleChange(value) {
    this.setState({ selected: value });
  }
  priceChange(value) {
    this.setState({ pricingselected: value });
  }

  handleClick() {
    this.setState({ hasError: false });
    if (!this.state.selected) {
      this.setState({ hasError: true });
    }
  }
  handleEvent(event, picker) {
    console.log(picker.startDate);
  }
  render() {
    const { classes } = this.props;
    const { selected, pricingselected, hasError } = this.state;

    return (
      <div>
        <SidebarMenu />
        <div className="Content">
          <Tabs defaultTab="TRENDS" horizontal>
            <div className="filter">
              <p>Filter results by:</p>

              {/* <form id="data" autoComplete="off" className="pricing_cycle">
                <FormControl error={hasError}>
                  <TextField
                    select
                    variant="outlined"
                    value={pricingselected}
                    onChange={event => this.priceChange(event.target.value)}
                    input={<Input id="name" name="name" />}
                  >
                    <MenuItem
                      classes={{ pricingselected: classes.MenuItemSelected }}
                      className={classes.MenuItem}
                      value="Pricing"
                    >
                      Pricing Cycle
                    </MenuItem>
                    <MenuItem
                      classes={{ pricingselected: classes.MenuItemSelected }}
                      className={classes.MenuItem}
                      value="View all (default)"
                    >
                      View all (default)
                    </MenuItem>
                    <MenuItem
                      classes={{ pricingselected: classes.MenuItemSelected }}
                      className={classes.MenuItem}
                      value="09:00 AM"
                    >
                      09:00 AM
                    </MenuItem>
                    <MenuItem
                      classes={{ pricingselected: classes.MenuItemSelected }}
                      className={classes.MenuItem}
                      value="01:00 PM"
                    >
                      01:00 PM
                    </MenuItem>
                  </TextField>
                  {hasError && (
                    <FormHelperText>This is required!</FormHelperText>
                  )}
                </FormControl>
              </form> */}

              <DateRangePicker
                showDropdowns
                onEvent={this.handleEvent}
                endDate="3/1/2014">
                <div className="date_main">
                  <img src={icCal} className="ic_name" />
                  <span className="date_lable">From:</span> 3/1/2014{" "}
                  <span className="date_lable">To:</span> 3/1/2014
                </div>
              </DateRangePicker>
            </div>

            <TabList>
              <Tab tabFor="TRENDS" className="trends">
                PRICING TRENDS
              </Tab>
              <Tab tabFor="COMPARE" className="compare">
                COMPARE PRICES
              </Tab>
              <Tab tabFor="AUDIT" className="audit">
                AUDIT TRAIL
              </Tab>
              <Tab tabFor="" className="export">
                EXPORT REPORT
              </Tab>
            </TabList>

            <div className="">
              <TabPanel tabId="TRENDS">
                <div className="row">
                  <form
                    id="two"
                    autoComplete="off"
                    className="statistics_display">
                    <label>Displaying Statistics for: </label>
                    <FormControl error={hasError}>
                      <TextField
                        select
                        variant="outlined"
                        value={selected}
                        displayEmpty
                        onChange={event =>
                          this.handleChange(event.target.value)
                        }
                        input={<Input id="name" name="name" />}>
                        <MenuItem
                          classes={{ selected: classes.MenuItemSelected }}
                          className={classes.MenuItem}
                          value="APPLE">
                          APPLE IPHONE XR SMARTPHONE LTE WITH FACETIME, BLACK,
                          128 GB
                        </MenuItem>
                        <MenuItem
                          classes={{ selected: classes.MenuItemSelected }}
                          className={classes.MenuItem}
                          value="IPHONE">
                          APPLE IPHONE XR SMARTPHONE LTE WITH FACETIME, BLACK,
                          128 GB
                        </MenuItem>
                        <MenuItem
                          classes={{ selected: classes.MenuItemSelected }}
                          className={classes.MenuItem}
                          value="XR">
                          APPLE IPHONE XR SMARTPHONE LTE WITH FACETIME, BLACK,
                          128 GB
                        </MenuItem>
                      </TextField>
                      {hasError && (
                        <FormHelperText>This is required!</FormHelperText>
                      )}
                    </FormControl>
                  </form>
                  <div className="col-md-12">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={options}
                    />
                  </div>
                </div>
              </TabPanel>
              <TabPanel tabId="COMPARE">
                <Table />
              </TabPanel>
              <TabPanel tabId="AUDIT">
                <AuditTable />
              </TabPanel>
            </div>
          </Tabs>
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(Content);
