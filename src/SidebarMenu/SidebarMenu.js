import React, { Component } from "react";
import { Tabs, Tab, TabPanel, TabList } from "react-web-tabs";
import "react-web-tabs/dist/react-web-tabs.css";
import "./SidebarMenu.scss";
import HeaderAfterLogin from "./../HeaderAfterLogin/HeaderAfterLogin";

// images
import IconSearch from "./../public/images/SidebarMenu/ic_search.svg";

// import iconSignOut from './../public/images/SidebarMenu/ic-sign-out.svg';

class SidebarMenu extends Component {
  // constructor(){
  //     super(props)
  //     this.state = { active: '' };
  // }
  render() {
    //   handleonchaneg(){
    //       this.setState.
    //   }
    return (
      <div>
        <HeaderAfterLogin />

        <div className="sidebar-main">
          <h4>
            VIEW BY <br /> CATEGORIES
          </h4>
          <Tabs defaultTab="mobiles" vertical>
            <TabList>
              <Tab tabFor="mobiles">MOBILES</Tab>
              <Tab tabFor="computers_tablets">COMPUTERS & TABLETS</Tab>
              <Tab tabFor="networking_smart_devices">
                NETWORKING & SMART DEVICES
              </Tab>
              <Tab tabFor="personal_care_applicances">
                PERSONAL CARE & APPLICANCES
              </Tab>
              <Tab tabFor="television_home_theaters">
                TELEVISION & HOME THEATERS
              </Tab>
              <Tab tabFor="cameras">CAMERAS</Tab>
              <Tab tabFor="gaming">GAMING</Tab>
              <Tab tabFor="wearables">WEARABLES</Tab>
              <Tab tabFor="headphones_speakers">HEADPHONES & SPEAKERS</Tab>
              <Tab tabFor="accessories">ACCESSORIES</Tab>
            </TabList>
            <div className="subsidemenu">
              <TabPanel tabId="mobiles">
                <div className="mobile_search">
                  <img src={IconSearch} className="ic_serach" alt="" />
                  <input type="text" name="lastname" />
                </div>
                <span>Apple</span>
                <span>Samsung</span>
                <span>Huawei</span>
                <span>Honor</span>
                <span>Xiaomi</span>
                <span>Oppo</span>
                <span>Nokia</span>
                <span>Lava</span>
                <span>BlackBerry</span>
                <span>Lenovo</span>
                <span>ZTEP</span>
              </TabPanel>
              <TabPanel tabId="computers_tablets">
                <p>Tab 3 content</p>
              </TabPanel>
              <TabPanel tabId="networking_smart_devices">
                <p>Tab 3 content</p>
              </TabPanel>
              <TabPanel tabId="personal_care_applicances">
                <p>Tab 3 content</p>
              </TabPanel>
              <TabPanel tabId="television_home_theaters">
                <p>Tab 3 content</p>
              </TabPanel>
              <TabPanel tabId="cameras">
                <p>Tab 3 content</p>
              </TabPanel>
              <TabPanel tabId="gaming">
                <p>Tab 3 content</p>
              </TabPanel>
              <TabPanel tabId="wearables">
                <p>Tab 3 content</p>
              </TabPanel>
              <TabPanel tabId="headphones_speakers">
                <p>Tab 3 content</p>
              </TabPanel>
              <TabPanel tabId="accessories">
                <p>Tab 3 content</p>
              </TabPanel>
            </div>
          </Tabs>
        </div>
      </div>
    );
  }
}
export default SidebarMenu;
