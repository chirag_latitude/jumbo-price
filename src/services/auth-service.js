export const signIn = body => {
  return fetch({
    method: "POST",
    url: `https://reqres.in/api/login`,
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(body)
  });
};
